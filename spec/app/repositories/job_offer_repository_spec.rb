require 'integration_spec_helper'
require_relative '../../../app/errors/invalid_owner'

describe JobOfferRepository do
  let(:repository) { described_class.new }

  let(:owner) do
    user = User.new(name: 'Joe', email: 'joe@doe.com', crypted_password: 'secure_pwd')
    UserRepository.new.save(user)
    user
  end

  let!(:today_offer) do
    today_offer = JobOffer.new(title: 'a title',
                               updated_on: Date.today,
                               is_active: true,
                               user_id: owner.id)
    repository.save(today_offer)
    today_offer
  end

  describe 'deactive_old_offers' do
    let!(:thirty_day_offer) do
      thirty_day_offer = JobOffer.new(title: 'a title',
                                      updated_on: Date.today - 45,
                                      is_active: true,
                                      user_id: owner.id)
      repository.save(thirty_day_offer)
      thirty_day_offer
    end

    it 'should deactivate offers updated 45 days ago' do
      repository.deactivate_old_offers

      updated_offer = repository.find(thirty_day_offer.id)
      expect(updated_offer.is_active).to eq false
    end

    it 'should not deactivate offers created today' do
      repository.deactivate_old_offers

      not_updated_offer = repository.find(today_offer.id)
      expect(not_updated_offer.is_active).to eq true
    end
  end

  describe 'find_for_user' do
    it 'should raise Foribidden error when i acces anothers offer' do
      another_user = User.new(name: 'Jane', email: 'jane@doe.com', crypted_password: 'secure_pwd')
      UserRepository.new.save(another_user)
      expect { repository.find_for_user(today_offer.id, another_user) }
        .to raise_error(InvalidOwner)
    end
  end
end
